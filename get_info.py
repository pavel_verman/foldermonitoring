# -- coding: utf-8 --

from __future__ import print_function

from twisted.internet import reactor, protocol
from twisted.protocols.basic import LineReceiver
import sys
import json
from datetime import datetime as dt
from datetime import timedelta

DEFAULT_HOST_PORT = 9844
DEFAULT_HOST_IP = "localhost"
TIME_TEMPLATE = "%Y-%m-%dT%H:%M:%S"


class InfoClient(LineReceiver):

    def connectionMade(self):
        print ("Connection started")
        # Посылаем запрос на сервер
        json_string = json.dumps({'get_info': {'client_name': self.factory.client_name,
                                               'date_from': self.factory.date_from,
                                               'date_to': self.factory.date_to}},
                                 encoding="utf-8")
        self.sendLine(json_string)

    def lineReceived(self, data):
        # Данные от сервера получены
        if data == "CLOSE":
            self.transport.loseConnection()
        else:
            print(data)

class Factory(protocol.ClientFactory):
    protocol = InfoClient

    def __init__(self, client_name_param, date_from, date_to):
        if client_name_param:
            self.client_name = client_name_param
        if date_from:
            self.date_from = date_from
        if date_to:
            self.date_to = date_to

    def clientConnectionFailed(self, connector, reason):
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        reactor.stop()


if __name__ == '__main__':
    # sys.argv[0] - script name
    # sys.argv[1] - client name
    # sys.argv[2] - start date in format YYYY-mm-ddTHH:MM:SS
    # sys.argv[3] - end date in format YYYY-mm-ddTHH:MM:SS
    # sys.argv[4] - host IP
    # sys.argv[5] - host PORT

    host = DEFAULT_HOST_IP
    port = DEFAULT_HOST_PORT
    client_name = sys.argv[1] if len(sys.argv) > 1 else ''
    date_from = sys.argv[2] if len(sys.argv) > 2 else (dt.now() - timedelta(days=1)).strftime(TIME_TEMPLATE)
    date_to = sys.argv[3] if len(sys.argv) > 3 else dt.now().strftime(TIME_TEMPLATE)
    host = sys.argv[4] if len(sys.argv) > 4 else DEFAULT_HOST_IP
    port = sys.argv[5] if len(sys.argv) > 5 else DEFAULT_HOST_PORT
    f = Factory(client_name, date_from, date_to)
    reactor.connectTCP(host, port, f)
    reactor.run()
