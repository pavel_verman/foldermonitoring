# -- coding: utf-8 --

from __future__ import print_function

from twisted.internet import reactor, protocol
import uuid
import os,  sys
import threading
import json
from ReportTypes import ReportTypes
from twisted.protocols.basic import LineReceiver

DEFAULT_HOST_PORT = 9844
DEFAULT_HOST_IP = "localhost"


class Client(LineReceiver):

    # Информация о файлах каталога. Для простоты храним как ключ-значение, где
    # ключ - полное имя файла, значение - размер файла в байтах
    # Из задания следует, что отслеживать надо не изменение файла, а изменение только его размера
    files_info = {}
    cur_report = []
    is_closed = False
    # Сканирование производится каждые $SCAN_PERIOD_TIME секунд
    # Период устанавливается после завершения предыдущего сканирования
    SCAN_PERIOD_TIME = 2

    def connectionMade(self):
        print ("Connection started")
        # Первое сообщение всегда имя Клиента
        json_string = json.dumps({'report': {'type': ReportTypes.HELLO, 'name': self.factory.client_name}}, encoding="utf-8")
        self.sendLine(json_string)
    
    def lineReceived(self, data):
        # Получает команды от сервера
        if 'START' in data:
            # Начинаем отсылать инфомрацию о файлах
            print ("Start reporting")
            self.scan_directory()
        elif 'STOP' in data:
            print ("Stoping reporting...")
            self.transport.loseConnection()
            self.is_closed = True

    def send_report(self, mess_type, body):
        # Отправляем инфомрацию о файле на сервер
        json_string = json.dumps({'report': {'type': mess_type, 'name': self.factory.client_name, 'info': body}}, encoding="utf-8")
        self.sendLine(json_string)

    def scan_directory(self):
        # Сканирование с помощью функции os.walk короче и проще нежели стандартное решение в виде рекурсивной функции
        if self.is_closed:
            sys.exit(1)
        self.cur_report = []
        new_scan_result = {}  # структура для хранения текущего состояния каталога
        for path, dirs, files in os.walk(self.factory.work_directory):
            #path - текущий каталог куда смотрит цикл (строка)
            #dirs - массив имен директорий в текущем каталоге
            #files - массив имен файлов в текущем каталоге
            for fname in files:
                file_path = os.path.join(path, fname).decode('cp1251')
                file_size = os.path.getsize(file_path)
                new_scan_result[file_path] = file_size
                if file_path not in self.files_info:
                    # Обнаружен новый файл
                    self.send_report(ReportTypes.NEW, {'fpath': file_path, 'fsize': file_size})
                else:
                    previous_size = self.files_info[file_path]
                    if file_size != previous_size:
                        # размер файла изменился
                        self.send_report(ReportTypes.DIF, {'fpath': file_path, 'fsize': file_size})
                    # помечаем файл как "прочитанный"
                    self.files_info.pop(file_path)
        # Оставшиеся файлы в self.files_info являются удаленными
        for fpath in self.files_info.keys():
            self.send_report(ReportTypes.DEL, {'fpath': fpath})
        # Обновляем информацию о текущем состоянии директории
        self.files_info = new_scan_result
        # Отправляем результат на сервер если есть изменения
        #if self.cur_report:
        #    json_string = json.dumps({'report': self.cur_report}, encoding="utf-8")
        #    self.transport.write(json_string)
        # Ставит следующий таймер для повторного сканирования дериктории
        threading.Timer(self.SCAN_PERIOD_TIME, self.scan_directory).start()


class Factory(protocol.ClientFactory):
    # Клиент возвращает изменение размеров файлов в директории work_directory
    # work_directory по умолчанию равна дирректории файла Клиент
    work_directory = os.path.dirname(os.path.abspath(__file__))
    # Если клиенту не задано имя, то генерируется уникальная строковая последовательность
    client_name = uuid.uuid4().hex
    protocol = Client

    def __init__(self, work_directory_param, client_name_param):
        if work_directory_param:
            self.work_directory = work_directory_param
        if client_name_param:
            self.client_name = client_name_param

    def clientConnectionFailed(self, connector, reason):
        reactor.stop()
    
    def clientConnectionLost(self, connector, reason):
        reactor.stop()


if __name__ == '__main__':
    # sys.argv[0] - script name
    # sys.argv[1] - host IP
    # sys.argv[2] - host PORT
    # sys.argv[3] - folder
    # sys.argv[4] - client name
    host = DEFAULT_HOST_IP
    port = DEFAULT_HOST_PORT
    host = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_HOST_IP
    port = sys.argv[2] if len(sys.argv) > 2 else DEFAULT_HOST_PORT
    work_directory = sys.argv[3] if len(sys.argv) > 3 else ''
    client_name = sys.argv[4] if len(sys.argv) > 4 else ''
    f = Factory(work_directory, client_name)
    reactor.connectTCP(host, port, f)
    reactor.run()
