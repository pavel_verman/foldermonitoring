# -*- coding: utf-8 -*-


class ReportTypes:
    HELLO = 'hi'  # инициализация общения
    NEW = 'new'  # появился новый файл
    DIF = 'dif'  # у файла изменился размер
    DEL = 'del'  # файл был удален
