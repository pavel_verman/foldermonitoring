# -- coding: utf-8 --

import sys
import TCPServer


DEFAULT_HOST_PORT = 9844

if __name__ == '__main__':
    # sys.argv[0] - script name
    # sys.argv[1] - host port
    port = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_HOST_PORT
    TCPServer.start_listen(port)
