# -- coding: utf-8 --

from __future__ import print_function

from twisted.internet.protocol import Factory
from twisted.internet import reactor
from twisted.protocols.basic import LineReceiver
import json
import time
from datetime import datetime as dt
from ReportTypes import ReportTypes
from FolderStructure import FolderStructure


class TCPServer(LineReceiver):

    clients_folder_structure = {}  # хранит соответствие между именем клиента и его FolderStructure
    TIME_TEMPLATE = "%Y-%m-%dT%H:%M:%S"
    RECORDS_ON_PAGE = 30  # параметр указывающий количество записей на странице
    DELAY_PER_NEW_PAGE = 1  # задержка перед отправкой новой страницы 0.1 сек

    def lineReceived(self, data):
        try:
            #print (data)
            data = json.loads(data, encoding="utf-8")
            if type(data) == dict and 'get_info' in data:
                # API-запрос на получение информации о клиенте
                request_params = self.check_data_key('get_info', data)
                client_name = self.check_data_key('client_name', request_params)
                date_from = self.check_data_key('date_from', request_params)
                date_to = self.check_data_key('date_to', request_params)
                # check input dates
                date_from = dt.strptime(date_from, self.TIME_TEMPLATE)
                date_to = dt.strptime(date_to, self.TIME_TEMPLATE)
                if client_name in self.clients_folder_structure:
                    records = self.clients_folder_structure[client_name].get_info(date_from, date_to)
                    if records:
                        reactor.callFromThread(self.paged_response, records)
                    else:
                        self.sendLine("Empty response")
                else:
                    # Записи о данном клиенте не обнаружены
                    self.sendLine("Error. Unknown client name")
            else:
                # Получено сообщение от клиента
                body_report = self.check_data_key('report', data)
                message_type = self.check_data_key('type', body_report)
                client_name = self.check_data_key('name', body_report)
                if message_type == ReportTypes.HELLO:
                    # Инициализация структуры для хранения информации о клиенте
                    self.clients_folder_structure[client_name] = FolderStructure(client_name)
                    print ('New client: ', client_name)
                    self.sendLine('START')
                elif message_type == ReportTypes.NEW or message_type == ReportTypes.DIF:
                    if client_name in self.clients_folder_structure:
                        cur_time = dt.now().strftime(self.TIME_TEMPLATE)
                        data_info = self.check_data_key('info', body_report)
                        file_path = self.check_data_key('fpath', data_info)
                        file_size = self.check_data_key('fsize', data_info)
                        self.clients_folder_structure[client_name].update_file_info(file_path, file_size, cur_time)
                        print ('%s (%s): %s | size: %s bytes | time: %s' % (
                        client_name, message_type, file_path, file_size, cur_time))
                elif message_type == ReportTypes.DEL:
                    if client_name in self.clients_folder_structure:
                        data_info = self.check_data_key('info', body_report)
                        file_path = self.check_data_key('fpath', data_info)
                        self.clients_folder_structure[client_name].delete_file(file_path)
                        print ('%s (%s): %s' % (client_name, message_type, file_path))
        except ValueError:
            self.transport.write('STOP Connection. Incorrect package format')
            self.transport.loseConnection()

    def check_data_key(self, data_key, data_dict):
        if data_key in data_dict:
            return data_dict[data_key]
        else:
            # Получены не корректные данные!
            self.sendLine('STOP Connection. Incorrect package structure')
            self.transport.loseConnection()

    def paged_response(self, records):
        # Постраничного вывода нет. Выводится построчно
        # TODO постраничный вывод
        cur_index = 0
        while cur_index < len(records):
            self.sendLine(records[cur_index].encode('cp1251'))
            cur_index += 1
            #if cur_index % self.RECORDS_ON_PAGE == 0:
            #    time.sleep(self.DELAY_PER_NEW_PAGE)
        self.sendLine("CLOSE")


def start_listen(port):
    f = Factory()
    f.protocol = TCPServer
    reactor.listenTCP(port, f)
    reactor.run()
