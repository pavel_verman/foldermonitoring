# -*- coding: utf-8 -*-

from datetime import datetime as dt


class FolderStructure:
    # Информация о файлах каталога. Для простоты храним как ключ-значение, где
    # ключ - полное имя файла, значение - структура с: размером файла, временем обновления записи
    files_info = {}
    TIME_TEMPLATE = "%Y-%m-%dT%H:%M:%S"

    def __init__(self, client_name):
        self.client_name = client_name  # Пусть идентификация производится по имени клиента

    def update_file_info(self, file_path, file_size, receipt_time):
        # Следует добавить или обновить запись о файле
        self.files_info[file_path] = {'size': file_size, 'time': receipt_time}

    def delete_file(self, file_path):
        self.files_info.pop(file_path, None)

    def get_info(self, date_from, date_to):
        filtered_info = []
        for file_path, file_info in self.files_info.items():
            time_target = dt.strptime(file_info['time'], self.TIME_TEMPLATE)
            if date_from <= time_target <= date_to:
                filtered_info.append('%s | size: %s bytes | time: %s' % (file_path, file_info['size'], file_info['time']))
        return filtered_info
