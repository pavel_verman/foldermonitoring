# Описание

Набор скриптов для рекурсивного отслеживания изменений размера файлов в каталоге.
Реализовано на Python 2.7 с поддержкой версий 3.* (не тестировалось)

Необходимо подключить библиотеку **twisted**

## Особенности
- Клиент-серверная архитектура
- Поддержка кириллицы в имени файлов
- Использован пакет twisted для создания TCP-соединения
- Регистрируются все файлы во вложенных дирректориях
- Реализовано API для получения информации о текущем состоянии дирректории клиента
- Для запуска скриптов необходим интерпретатор Python
- Почти все параметры генерируются по умолчанию, в случае их отсутствия

## Компонент: Сервер

**Server/StartMonitor.py**  -  скрипт для запуска сервера. Запускается на хосте с доступным IP-адресом.

Производит вывод в консоль информации об измененях в клиентской директории.

В случае возникновения ошибки при обработки запроса, сервер **разрывает соединение**.

При успешной обработки информации от клиента вывод сообщений в консоль осуществляется в следующем формате:

{CLIENT_NAME} ({MESSAGE_TYPE}): {FILE_PATH}| size: {FILE_SIZE} bytes | time: {TIMESTAMP}

- CLIENT_NAME - имя клиента
- MESSAGE_TYPE - тип сообщения: new - новый файл, dif - изменился размер файла, del - файл удален
- FILE_PATH - абсолютный путь к файлу на клиентской машине
- FILE_SIZE - размер файла указанный в байтах
- TIMESTAMP - время сообщения (фиксирования измениня) в формате YYYY-mm-ddTHH:MM:SS, где T - разделитель(англ. знак T)

Параметры:

1. host_port : порт для подключения. По умолчанию: **9844**

## Компонент: Клиент

**Client/StartMonitor.py** - скрипт для запуска клиента. После запуска осуществляется сканирование указанной
директории каждые 2 секунды (параметры может быть изменен). 

Отслеживаются следующие события: создание нового, изменение размера, удаление файла.

Параметры:

1. host_ip : IP-адрес хоста для подключения. По умолчанию: localhost
2. host_port : порт для подключения к серверу. По умолчанию: 9844
3. odirectory : абсолютный путь директории, вида C:\same_dir\. Указывает директорию для сканирования. По умолчанию: директория запуска скрипта
4. client_name : имя клиента. Данное имя должно быть уникальным среди всех остальных клиентов. По умолчанию: генерируется уникальная символьная последовательность

## Компонент: API-запрос
**get_info.py** - скрипт для получения информации о текущем состоянии дирректории клиента.

Параметры:

1. client_name : имя клиента.
2. start_date : время и дата начала периода в формате YYYY-mm-ddTHH:MM:SS. По умолчанию: вчерашняя дата с текущем временем (день назад).
3. end_date : время и дата окончания периода в формате YYYY-mm-ddTHH:MM:SS. По умолчанию: текущее время и дата.
4. host_ip : IP-адрес хоста для подключения. По умолчанию: localhost
5. host_port : порт для подключения к серверу. По умолчанию: 9844

Запрос возвращает ошибку в случае отсутствия имени клиента в текущей сессии сервера.

В случае успешной обработки запроса, в консоле отобразятся записи о файлах клиента за указанный период времени в следующем формате:

{FILE_PATH} | size: {FILE_SIZE} bytes | time: {TIMESTAMP}

- FILE_PATH - абсолютный путь к файлу на клиентской машине
- FILE_SIZE - размер файла указанный в байтах
- TIMESTAMP - время сообщения (фиксирования измениня) в формате YYYY-mm-ddTHH:MM:SS, где T - разделитель(англ. знак T)
